﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz1Marcos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double consumo;
            double tarifa;
            double auxiliar;
            double auxiliar2;

            consumo = double.Parse(txt_kwh.Text);

            if (consumo < 12)
                tarifa = 10;

            else if (consumo > 12 || consumo < 65)
            { auxiliar = consumo - 11;
            tarifa = (auxiliar * 2) + 10; }

            else
            {
           
                tarifa = (consumo * 4) + 10;
            }

            txt_Resultado.Text = string.Format("{0:F2}", tarifa);

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            txt_kwh.Text = "";
            txt_Resultado.Text = "";
        }

        private void btn_calcular_edad_Click(object sender, EventArgs e)
        {
            double edad;

            edad = double.Parse(txt_edad.Text);

            if (edad >= 16 && edad <= 18)
            { 
                txt_edificio.Text = "A";
                txt_estado_edad.Text = "aceptado";
                }
            else if (edad >= 19 && edad <= 20)
            {
                txt_edificio.Text = "B";
                txt_estado_edad.Text = "aceptado";
            }
            else if (edad >= 21 && edad <= 25)
            {
                txt_edificio.Text = "C";
                txt_estado_edad.Text = "aceptado";
            }
            else
                txt_estado_edad.Text = "no es aceptado";


        }

        private void btn_limpiar_edad_Click(object sender, EventArgs e)
        {
            txt_edad.Text = "";
            txt_edificio.Text = "";
            txt_estado_edad.Text = "";
        }

        private void btn_salir_edad_Click(object sender, EventArgs e)
        {
            Close();

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
