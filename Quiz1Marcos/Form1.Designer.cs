﻿namespace Quiz1Marcos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_calcular_tarifaluz = new System.Windows.Forms.Button();
            this.btn_limpiar_tarifaluz = new System.Windows.Forms.Button();
            this.btn_salir__tarifaluz = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_kwh = new System.Windows.Forms.TextBox();
            this.txt_Resultado = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_verificar_edad = new System.Windows.Forms.Button();
            this.txt_edificio = new System.Windows.Forms.TextBox();
            this.btn_limpiar_edad = new System.Windows.Forms.Button();
            this.txt_edad = new System.Windows.Forms.TextBox();
            this.btn_salir_edad = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_estado_edad = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_D = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txt_X = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_calcular_tarifaluz
            // 
            this.btn_calcular_tarifaluz.Location = new System.Drawing.Point(25, 143);
            this.btn_calcular_tarifaluz.Name = "btn_calcular_tarifaluz";
            this.btn_calcular_tarifaluz.Size = new System.Drawing.Size(75, 23);
            this.btn_calcular_tarifaluz.TabIndex = 0;
            this.btn_calcular_tarifaluz.Text = "Calcular";
            this.btn_calcular_tarifaluz.UseVisualStyleBackColor = true;
            this.btn_calcular_tarifaluz.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_limpiar_tarifaluz
            // 
            this.btn_limpiar_tarifaluz.Location = new System.Drawing.Point(130, 143);
            this.btn_limpiar_tarifaluz.Name = "btn_limpiar_tarifaluz";
            this.btn_limpiar_tarifaluz.Size = new System.Drawing.Size(75, 24);
            this.btn_limpiar_tarifaluz.TabIndex = 1;
            this.btn_limpiar_tarifaluz.Text = "Limpiar";
            this.btn_limpiar_tarifaluz.UseVisualStyleBackColor = true;
            this.btn_limpiar_tarifaluz.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_salir__tarifaluz
            // 
            this.btn_salir__tarifaluz.Location = new System.Drawing.Point(229, 143);
            this.btn_salir__tarifaluz.Name = "btn_salir__tarifaluz";
            this.btn_salir__tarifaluz.Size = new System.Drawing.Size(75, 23);
            this.btn_salir__tarifaluz.TabIndex = 2;
            this.btn_salir__tarifaluz.Text = "Salir";
            this.btn_salir__tarifaluz.UseVisualStyleBackColor = true;
            this.btn_salir__tarifaluz.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Kw/H";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Resultado";
            // 
            // txt_kwh
            // 
            this.txt_kwh.Location = new System.Drawing.Point(126, 25);
            this.txt_kwh.Name = "txt_kwh";
            this.txt_kwh.Size = new System.Drawing.Size(100, 22);
            this.txt_kwh.TabIndex = 5;
            // 
            // txt_Resultado
            // 
            this.txt_Resultado.Location = new System.Drawing.Point(126, 76);
            this.txt_Resultado.Name = "txt_Resultado";
            this.txt_Resultado.Size = new System.Drawing.Size(100, 22);
            this.txt_Resultado.TabIndex = 6;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(2, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(350, 296);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btn_calcular_tarifaluz);
            this.tabPage1.Controls.Add(this.txt_Resultado);
            this.tabPage1.Controls.Add(this.btn_limpiar_tarifaluz);
            this.tabPage1.Controls.Add(this.txt_kwh);
            this.tabPage1.Controls.Add(this.btn_salir__tarifaluz);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(386, 226);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tarifa de luz";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txt_estado_edad);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.btn_verificar_edad);
            this.tabPage2.Controls.Add(this.txt_edificio);
            this.tabPage2.Controls.Add(this.btn_limpiar_edad);
            this.tabPage2.Controls.Add(this.txt_edad);
            this.tabPage2.Controls.Add(this.btn_salir_edad);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(386, 226);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Alumnos";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_verificar_edad
            // 
            this.btn_verificar_edad.Location = new System.Drawing.Point(21, 174);
            this.btn_verificar_edad.Name = "btn_verificar_edad";
            this.btn_verificar_edad.Size = new System.Drawing.Size(75, 23);
            this.btn_verificar_edad.TabIndex = 7;
            this.btn_verificar_edad.Text = "Verificar";
            this.btn_verificar_edad.UseVisualStyleBackColor = true;
            this.btn_verificar_edad.Click += new System.EventHandler(this.btn_calcular_edad_Click);
            // 
            // txt_edificio
            // 
            this.txt_edificio.Location = new System.Drawing.Point(122, 81);
            this.txt_edificio.Name = "txt_edificio";
            this.txt_edificio.Size = new System.Drawing.Size(100, 22);
            this.txt_edificio.TabIndex = 13;
            // 
            // btn_limpiar_edad
            // 
            this.btn_limpiar_edad.Location = new System.Drawing.Point(126, 174);
            this.btn_limpiar_edad.Name = "btn_limpiar_edad";
            this.btn_limpiar_edad.Size = new System.Drawing.Size(75, 24);
            this.btn_limpiar_edad.TabIndex = 8;
            this.btn_limpiar_edad.Text = "Limpiar";
            this.btn_limpiar_edad.UseVisualStyleBackColor = true;
            this.btn_limpiar_edad.Click += new System.EventHandler(this.btn_limpiar_edad_Click);
            // 
            // txt_edad
            // 
            this.txt_edad.Location = new System.Drawing.Point(122, 30);
            this.txt_edad.Name = "txt_edad";
            this.txt_edad.Size = new System.Drawing.Size(100, 22);
            this.txt_edad.TabIndex = 12;
            // 
            // btn_salir_edad
            // 
            this.btn_salir_edad.Location = new System.Drawing.Point(225, 174);
            this.btn_salir_edad.Name = "btn_salir_edad";
            this.btn_salir_edad.Size = new System.Drawing.Size(75, 23);
            this.btn_salir_edad.TabIndex = 9;
            this.btn_salir_edad.Text = "Salir";
            this.btn_salir_edad.UseVisualStyleBackColor = true;
            this.btn_salir_edad.Click += new System.EventHandler(this.btn_salir_edad_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Edificio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Edad";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "Estado";
            // 
            // txt_estado_edad
            // 
            this.txt_estado_edad.Location = new System.Drawing.Point(122, 126);
            this.txt_estado_edad.Name = "txt_estado_edad";
            this.txt_estado_edad.Size = new System.Drawing.Size(100, 22);
            this.txt_estado_edad.TabIndex = 15;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.textBox1);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.txt_D);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.txt_X);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(342, 267);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Oficina de Corres";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(24, 209);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Calcular";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // txt_D
            // 
            this.txt_D.Location = new System.Drawing.Point(150, 90);
            this.txt_D.Name = "txt_D";
            this.txt_D.Size = new System.Drawing.Size(100, 22);
            this.txt_D.TabIndex = 13;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(129, 209);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 24);
            this.button2.TabIndex = 8;
            this.button2.Text = "Limpiar";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // txt_X
            // 
            this.txt_X.Location = new System.Drawing.Point(150, 39);
            this.txt_X.Name = "txt_X";
            this.txt_X.Size = new System.Drawing.Size(100, 22);
            this.txt_X.TabIndex = 12;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(228, 209);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "Salir";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "D";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "X";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(150, 141);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "Valor Final de D";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 308);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Quiz";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_calcular_tarifaluz;
        private System.Windows.Forms.Button btn_limpiar_tarifaluz;
        private System.Windows.Forms.Button btn_salir__tarifaluz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_kwh;
        private System.Windows.Forms.TextBox txt_Resultado;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btn_verificar_edad;
        private System.Windows.Forms.TextBox txt_edificio;
        private System.Windows.Forms.Button btn_limpiar_edad;
        private System.Windows.Forms.TextBox txt_edad;
        private System.Windows.Forms.Button btn_salir_edad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_estado_edad;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_D;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txt_X;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox1;
    }
}

