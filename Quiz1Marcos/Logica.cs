﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Marcos
{
    class Logica
    {
        public double TarifaLuz(double consumo)
        {


            double tarifa;
            double auxiliar;
            double auxiliar2;

            if (consumo < 12)
                tarifa = 10;

            else if (consumo > 12 || consumo < 65)
            {
                auxiliar = consumo - 11;
                tarifa = (auxiliar * 2) + 10;
            }

            else
            {

                tarifa = (consumo * 4) + 10;
            }
            return tarifa;
        }

        public string edad(double edad)
        {
            string edificio;
            string estado;
            if (edad >= 16 && edad <= 18)
            {
                edificio = "A";
                estado = "aceptado";
                return edificio;

            }
            else if (edad >= 19 && edad <= 20)
            {
                edificio = "B";
                estado = "aceptado";
                return edificio;
            }
            else if (edad >= 21 && edad <= 25)
            {
                edificio = "C";
                estado = "aceptado";
                return edificio;
            }
            else
                estado = "no es aceptado";

            return estado;
        }

        public double Valoresxd(double x, double d)
        {

            double porcentaje;
            if (x <= 1)
            {
                if (d >= 100)
                {
                    porcentaje = d * 0.20;
                    d = d + porcentaje;
                }
            }
            else if (1<x && x <= 2)
            {
                if (d >= 100)
                {
                    porcentaje = d * 0.30;
                    d = d + porcentaje;
                }
            }
            else if (2 < x && x <= 3)
            {
                if (d >= 100)
                {
                    porcentaje = d * 0.40;
                    d = d + porcentaje;
                }
            }
            else
            {
                porcentaje = d * 0.50;
                d = d + porcentaje;

            }
                return d; 
        }

        public double Gramos(double cantgramos)
        {
            double cantdemas;
            double precio;
            if (cantgramos <= 20)
                 precio= cantgramos*10.00;

            else if (cantgramos > 20)
            {
                cantdemas = cantgramos - 20;
                precio = (cantdemas * 2.00)+(20*10.00);
            }

            else
            {

                precio = cantgramos * 1.50;
            }
            

            return precio;
        }
    }
}
